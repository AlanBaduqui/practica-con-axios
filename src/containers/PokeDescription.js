import React from 'react';
import { Row, Descriptions, Col } from 'antd';
import PokeCard from '../components/PokeCard';
import Title from 'antd/lib/typography/Title';

function PokeDescription({ pokeImage, name, description, height, weight, types }) {
    return (
        <>
            <Row align="middle" >
                <PokeCard image={pokeImage} />
                <Col span={12}>
                    <Descriptions title={name}>
                        <Title level={4}>
                            {description}
                        </Title>
                    </Descriptions>
                    <Descriptions title='Stats'>
                        <Title level={4}>
                            Altura: {height} ft
                        </Title>
                    </Descriptions>
                    <Descriptions>
                        <Title levle={4}>
                            Peso: {weight} lbr
                        </Title>
                    </Descriptions>
                    <Descriptions>
                    <Title level={4}>
                           Tipo: {types.map((item) =>{
                                return(
                                    <Title level={4}>
                                        {item.type.name}
                                    </Title>
                                );
                            })}
                        </Title>
                    </Descriptions>
                </Col>
            </Row>
        </>
    );
}

export default PokeDescription;