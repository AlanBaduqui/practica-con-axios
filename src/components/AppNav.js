import React from 'react'
import Title from 'antd/lib/typography/Title';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

function AppNav() {

    return (

        <Menu mode="horizontal" style={{background:'Red'}}>
            <Menu.Item key="">
                <Link to=''>
                    <Title level={4}>PokeApp</Title>
                </Link>
            </Menu.Item>
        </Menu>

    );
}

export default AppNav;