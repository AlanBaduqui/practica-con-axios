import React, { Fragment } from 'react';
import PokeCard from './PokeCard';
import { Row, Divider } from 'antd';

function List({ pokeData }) {
    return (
        <Fragment>
            <Divider orientation="left" style={{ color: '#333', fontWeight: 'normal' }}>
                <h1>Lista de pokemon</h1>
            </Divider>
            <Row justify='center'>
                {pokeData.map(pokemon => {
                    let url = '../official-artwork/'
                    let pokeIndex = pokemon.url.split('/')[pokemon.url.split('/').length - 2]

                    return <PokeCard to={`/poke-info/${pokeIndex}/${pokemon.name}`} name={pokemon.name} image={`${url}${pokeIndex}.png`} />
                })}
            </Row>
        </Fragment>
    );
}

export default List;