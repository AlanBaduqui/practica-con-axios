import React, { Component } from 'react';
import PokeDescription from './PokeDescription';
import AppNav from '../components/AppNav'

import axios from 'axios';

class PokeInfoContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pokemonDescription: "",
            height :"",
            weight: "",
            types: [],
        };
    }

    componentDidMount() {

        const { match } = this.props;
        const pokeID = match.params.pokeIndex;
        const PokeDescriptionUrl = `${process.env.REACT_APP_POKE_API_BASE_URL}pokemon-species/${pokeID}/`;
        this.getPokeStats();
        axios.get(PokeDescriptionUrl)
            .then(res => {
                const { flavor_text_entries } = res.data
                this.setState({
                    pokemonDescription: flavor_text_entries[11].flavor_text
                });
            })

    }

    getPokeStats() {
        const { match } = this.props;
        const pokeID = match.params.pokeIndex;
        axios.get(`${process.env.REACT_APP_POKE_API_BASE_URL}pokemon/${pokeID}/`)
        .then(res => {
            const{ height, weight, types} = res.data;
            this.setState({
                height,
                weight,
                types
            })
        });
    }
    render() {

        const { match } = this.props;
        const pokeID = match.params.pokeIndex;
        let url = `${process.env.REACT_APP_POKEMON_ART}`
        const pokeName = match.params.pokeName;
        const { pokemonDescription, height, weight, types }= this.state;

        return (
            <>
                <AppNav/>
                <PokeDescription 
                name={pokeName} 
                pokeImage={`${url}${pokeID}.png`} 
                description={pokemonDescription} 
                height={height}
                weight={weight}
                types={types}
                />
            </>
        );
    }
}

export default PokeInfoContainer