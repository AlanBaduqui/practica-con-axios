import React, { Fragment } from 'react';
import { Button } from 'antd';

const Pag = ({ increment, decrement, page }) => {


    return (
        <Fragment>
            <Button type="primary" onClick={decrement}> anterior </Button>
            <Button>{page}</Button>
            <Button type="primary" onClick={increment}> siguiente </Button>
        </Fragment>
    );
}
export default Pag;