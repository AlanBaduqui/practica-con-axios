import React from 'react';
import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';


const Home = () => {

    return (
        <div className='App'>
        < header className="App-header">
            <img src={process.env.PUBLIC_URL + '/pokemon.png'} width='400' alt='poke' />
          <Title style={{color: 'white'}}>PokeApp</Title>
          <Button type="primary" href='/pokemons'>Ver Pokemons</Button>
        </header>
      </div>
    );

}

export default Home;