import React from 'react';
import { Card} from 'antd';
import { Link } from 'react-router-dom';
import Meta from 'antd/lib/card/Meta';


function PokeCard({ name, image, to = '' }) {

    return (
        <Card style={{ width: 250, margin: "4em", }} cover={<img alt={name} src={image}/>}>
            <Link to={to}>
                <Meta
                    title={name}
                />
            </Link>
        </Card>
    );

}

export default PokeCard;